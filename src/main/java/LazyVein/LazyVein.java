package LazyVein;

import LazyVein.config.Config;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.IWorld;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.ToolType;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.regex.Pattern;

@Mod("lazyvein")
public class LazyVein {
    // Directly reference a log4j logger.
    private static final Logger LOGGER = LogManager.getLogger();

    public LazyVein() {
        // Register ourselves for server and other game events we are interested in
        MinecraftForge.EVENT_BUS.register(this);
        ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, Config.CLIENT_SPEC);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
    }

    private void setup(final FMLCommonSetupEvent event) {
        Config.bakeConfig();
    }

    // You can use EventBusSubscriber to automatically subscribe events on the contained class (this is subscribing to the MOD
    // Event bus for receiving Registry Events)
    @Mod.EventBusSubscriber
    public static class RegistryEvents {
        static private class HarvestedBlock {
            public final IWorld world;
            public final BlockPos pos;
            public final BlockState state;
            public final ServerPlayerEntity harvester;

            public HarvestedBlock(IWorld world, BlockPos pos, BlockState state, ServerPlayerEntity harvester) {
                this.world = world;
                this.pos = pos;
                this.state = state;
                this.harvester = harvester;
            }
        }
        private static ArrayList<HarvestedBlock> harvestedPositions = new ArrayList<>();
        private static boolean isFiredByVein = false;
        @SubscribeEvent
        public static void onBlockHarvested(BlockEvent.BreakEvent event) {
            if (!isFiredByVein) {
                final ToolType harvestTool = event.getState().getHarvestTool();
                if(harvestTool != null) {
                    if (Config.tooldict.contains(harvestTool.getName()) ^ Config.toolWhitelistMode) {
                        return;
                    }
                }
                ResourceLocation blockRegistry = event.getState().getBlock().getRegistryName();
                if (blockRegistry == null) {
                    return;
                }
                String blockRegistryName = blockRegistry.toString();
                if (Config.bakedOreDict) {
                    if (Config.oredict.contains(blockRegistryName) ^ Config.whitelistMode) {
                        return;
                    }
                } else {
                    for (Pattern p : Config.blockPatterns) {
                        if (p.matcher(blockRegistryName).matches() ^ Config.whitelistMode) {
                            return;
                        }
                    }
                }
                if (!(event.getPlayer() instanceof ServerPlayerEntity)) {
                    LOGGER.error("Block broken but Player not an instance of ServerPlayerEntity!");
                    return;
                }
            }
            ServerPlayerEntity serverPlayer = (ServerPlayerEntity) event.getPlayer();
            if (serverPlayer.isCreative()) {
                return;
            }
            if (serverPlayer.isCrouching()) {
                return;
            }
            ItemStack currentItem = serverPlayer.inventory.getCurrentItem();
            if (currentItem.getToolTypes().contains(event.getState().getHarvestTool())) {
                if (currentItem.getHarvestLevel(event.getState().getHarvestTool(), serverPlayer, event.getState()) != -1) {
                    harvestedPositions.add(new HarvestedBlock(event.getWorld(), event.getPos(), event.getState(), serverPlayer));
                }
            }
        }
        static private final Vec3i[] offsets = {
                new Vec3i(1, 0, 0), new Vec3i(-1, 0, 0),
                new Vec3i(0, 1, 0), new Vec3i(0, -1, 0),
                new Vec3i(0, 0, 1), new Vec3i(0, 0, -1)
        };

        @SubscribeEvent
        public static void ticker(final TickEvent.ServerTickEvent event) {
            if (event.phase == TickEvent.Phase.START) {
                return;
            }
            ArrayList<HarvestedBlock> processedHarvests = harvestedPositions;
            harvestedPositions = new ArrayList<>();

            for (HarvestedBlock p : processedHarvests) {
                for (Vec3i offset : offsets) {
                    BlockState currentBlockState = p.harvester.world.getBlockState(p.pos.add(offset));
                    if (!p.state.getBlock().equals(currentBlockState.getBlock())) {
                        continue;
                    }
                    ItemStack currentItem = p.harvester.inventory.getCurrentItem();
                    if (currentItem.getToolTypes().contains(currentBlockState.getHarvestTool())) {
                        if (currentItem.getHarvestLevel(currentBlockState.getHarvestTool(), p.harvester, currentBlockState) != -1) {
                            isFiredByVein = true;
                            p.harvester.interactionManager.tryHarvestBlock(p.pos.add(offset));
                            isFiredByVein = false;
                        }
                    }
                }
            }
        }
    }
}
