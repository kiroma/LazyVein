package LazyVein.config;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.registries.ForgeRegistries;
import org.apache.commons.lang3.tuple.Pair;

import java.util.*;
import java.util.regex.Pattern;

@Mod.EventBusSubscriber(modid = "lazyvein")
public class Config {

    public static final ClientConfig CLIENT;
    public static final ForgeConfigSpec CLIENT_SPEC;

    static {
        final Pair<ClientConfig, ForgeConfigSpec> specPair = new ForgeConfigSpec.Builder().configure(ClientConfig::new);
        CLIENT_SPEC = specPair.getRight();
        CLIENT = specPair.getLeft();
    }

    public static boolean whitelistMode;
    public static HashSet<String> oredict;
    public static ArrayList<Pattern> blockPatterns;
    public static HashSet<String> tooldict;
    public static boolean toolWhitelistMode;
    public static boolean bakedOreDict;

    public static void bakeConfig() {
        whitelistMode = CLIENT.whitelistMode.get();
        List<String> patterns = Arrays.asList(CLIENT.oredict.get().split(";"));
        bakedOreDict = CLIENT.bakedOreDict.get();
        blockPatterns = new ArrayList<>();
        blockPatterns.ensureCapacity(patterns.size());
        for (String s : patterns) {
            blockPatterns.add(Pattern.compile(s));
        }
        if (bakedOreDict) {
            oredict = new HashSet<>();
            Set<ResourceLocation> blockKeys = ForgeRegistries.BLOCKS.getKeys();
            for (ResourceLocation blockKey : blockKeys) {
                for (Pattern p : blockPatterns) {
                    if (p.matcher(blockKey.toString()).matches()) {
                        oredict.add(blockKey.toString());
                    }
                }
            }
        }
        toolWhitelistMode = CLIENT.toolWhitelistMode.get();
        tooldict = new HashSet<>(Arrays.asList(CLIENT.tooldict.get().split(";")));
    }

    @SubscribeEvent
    public static void onModConfigEvent(final ModConfig.ModConfigEvent configEvent) {
        if (configEvent.getConfig().getSpec() == CLIENT_SPEC) {
            bakeConfig();
        }
    }

    public static class ClientConfig {
        public final ForgeConfigSpec.BooleanValue whitelistMode;
        public final ForgeConfigSpec.ConfigValue<String> oredict;
        public final ForgeConfigSpec.BooleanValue bakedOreDict;
        public final ForgeConfigSpec.ConfigValue<String> tooldict;
        public final ForgeConfigSpec.BooleanValue toolWhitelistMode;
        public ClientConfig(ForgeConfigSpec.Builder builder) {
            whitelistMode = builder
                    .comment("Should the oredict act as a whitelist? (blacklist if false)")
                    .define("whitelistMode", false);
            oredict = builder
                    .comment("Pattern Strings separated by semicolons (\";\") to put into the whitelist/blacklist")
                    .define("oredict", "minecraft:stone;minecraft:cobblestone;minecraft:andesite;minecraft:diorite;minecraft:granite;minecraft:.*planks");
            bakedOreDict = builder
                    .comment("If set to true, blocks which can be veinmined are cached in memory, helpful if you have many complicated regex patterns.")
                    .define("bakedOreDict", true);
            tooldict = builder
                    .comment("Tool names separated by semicolons (\";\") to put into the whitelist/blacklist")
                    .define("tooldict", "");
            toolWhitelistMode = builder
                    .comment("If set to true, veinmine only works on specified tools.")
                    .define("toolWhitelistMode", false);
        }
    }
}
